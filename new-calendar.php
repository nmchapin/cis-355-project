<?php
/**
 * Displays the new calendar page
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

// Initialize the web application
include( 'includes/core.php' );
include( 'includes/func-calendar.php' );

ob_start();
session_start();

// Application error messages
$ApplicationErrors = array();

// If the user attempting to access this page is not logged in,
// then redirect him or her to the login page.
if ( ! isset( $_SESSION['user_id'] ) )
	exit( header( 'Location: login.php' ) );


// Check if the user pressed the "Create Calendar" button
if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

	// Form data
	$cal_name = ( isset( $_POST['name'] ) ? $_POST['name'] : "" );
	$cal_desc = ( isset( $_POST['description'] ) ? $_POST['description'] : "" );
	$cal_color = ( isset( $_POST['color'] ) ? intval( $_POST['color'] ) : -1 );

	// Sanitize textual form data
	$cal_name = filter_var( $cal_name, FILTER_SANITIZE_STRING );
	$cal_desc = filter_var( $cal_desc, FILTER_SANITIZE_STRING );

	// Form validity flag
	$FormDataValid = true;

	// Form error messages
	$FormErrors = array();

	// Make sure the user entered a valid calendar name
	if ( strlen( $cal_name ) == 0 ) {
		$FormDataValid = false;
		array_push( $FormErrors, 'You forgot to enter a name for the calendar.' );
	}

	// Make sure the user choose a valid calendar color
	if ( $cal_color < 0 || $cal_color > 6 ) {
		$FormDataValid = false;
		array_push( $FormErrors, 'You forgot to pick a color for the calendar.' );
	}

	// If the sanitized description entered by the user is a zero-length string, 
	// it means they probably entering HTML code.
	if ( strlen( $cal_desc ) == 0 & ( isset( $_POST['description'] ) && strlen( $_POST['description'] ) > 0 ) )
		array_push( $FormErrors, 'You entered HTML code into the description box. Please refrain from using any HTML tags.' );

	// If the user-submitted form data is valid, then save it to the database.
	if ( $FormDataValid ) {

		// Attempt to save the user's calendar to the database
		if ( ! create_calendar( $cal_name, $cal_desc, $cal_color, $_SESSION['user_id'] ) )
			array_push( $ApplicationErrors, 'There was a problem creating your calendar.' );
		else
			exit( header( 'Location: calendar.php' ) );

	}
}
?>
<!DOCTYPE html>
<html lang="en" class="default">
	<head>
		<meta charset="UTF-8">
		<title>Calendr</title>
		<meta name="description" content="Calendr is a free, easy-to-use event management system designed to help you keep track of life's important events.">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Style Sheets -->
		<link rel="stylesheet" href="css/style-backend.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="topbar" class="clearfix">
			<a href="/calendar.php">
				<img src="/img/logo-small.png" alt="" width="118" height="28">
			</a>
			<ul id="toolbar"><li><a href="#">&nbsp;</a></li></ul>
			<ul id="account">
				<li>
					<a href="#"><?php echo $_SESSION['user_name']; ?></a>
				</li>
			</ul>
		</div>
		<div id="content">
		<div id="content">
			<?php

			// Output all application-related error messages to the screen
			if ( isset( $ApplicationErrors ) && count( $ApplicationErrors ) > 0 ) {
				echo	'<div class="error-banner">
							<div>
								<p>The following errors occurred:</p>
								<ul>';
				
				foreach ( $ApplicationErrors as $Error )
					echo '<li>' . $Error . '</li>';
				
				echo 			'</ul>
						</div>
					</div>';
			}

			// Output all form-related error messages to the screen
			if ( isset( $FormErrors ) && count( $FormErrors ) > 0 ) {
				echo	'<div class="error-banner">
							<div>
								<p>Please correct the following issues:</p>
								<ul>';
				
				foreach ( $FormErrors as $Error )
					echo '<li>' . $Error . '</li>';
				
				echo 			'</ul>
						</div>
					</div>';
			}

			?>
			<div class="page-heading">Create New Calendar</div>
			<form method="post" action="/new-calendar.php">
				<ul>
					<li>
						<label for="name">Name</label>
						<input type="text" id="name" name="name"
						<?php if ( isset( $_POST['name'] ) ) echo "value='" . $_POST['name'] . "'"; ?>>
					</li>
					<li>
						<label for="description">Description</label>
						<textarea name="description" id="description" rows="5"><?php if ( isset( $_POST['description'] ) ) echo $_POST['description']; ?></textarea>
					</li>
					<li>
						<label>Color</label>
						<span class="color-palette">
							<input type="radio" name="color" value="0" <?php if ( isset( $_POST['color'] ) && $_POST['color'] == '0' ) echo 'checked'; ?>>
							<span class="cal-color-0">&nbsp;</span>
						</span>
						<span class="color-palette">
							<input type="radio" name="color" value="1" <?php if ( isset( $_POST['color'] ) && $_POST['color'] == '1' ) echo 'checked'; ?>>
							<span class="cal-color-1">&nbsp;</span>
						</span>
						<span class="color-palette">
							<input type="radio" name="color" value="2" <?php if ( isset( $_POST['color'] ) && $_POST['color'] == '2' ) echo 'checked'; ?>>
							<span class="cal-color-2">&nbsp;</span>
						</span>
						<span class="color-palette">
							<input type="radio" name="color" value="3" <?php if ( isset( $_POST['color'] ) && $_POST['color'] == '3' ) echo 'checked'; ?>>
							<span class="cal-color-3">&nbsp;</span>
						</span>
						<span class="color-palette">
							<input type="radio" name="color" value="4" <?php if ( isset( $_POST['color'] ) && $_POST['color'] == '4' ) echo 'checked'; ?>>
							<span class="cal-color-4">&nbsp;</span>
						</span>
						<span class="color-palette">
							<input type="radio" name="color" value="5" <?php if ( isset( $_POST['color'] ) && $_POST['color'] == '5' ) echo 'checked'; ?>>
							<span class="cal-color-5">&nbsp;</span>
						</span>
					</li>
				</ul>
				<input type="submit" class="button button-primary" value="Create Calendar">
				<a href="/calendar.php" class="button button-secondary">Cancel</a>
			</form>
		</div>
	</body>
</html>