<?php
/**
 * Displays the home page
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

// Initialize the web application
include( 'includes/core.php' );

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Calendr - Free Online Calendar</title>
		<meta name="description" content="Calendr is a free, easy-to-use event management system designed to help you keep track of life's important events.">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Style Sheets -->
		<link rel="stylesheet" href="/css/style.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="masthead">
			<div id="top-panel">
				<div class="clear fixed-width">
					<nav>
						<ul class="clear">
							<li><a href="/login.php">Sign In</a></li>
							<li>or</li>
							<li><a href="/register.php">Join Now</a></li>
						</ul>
					</nav>
				</div>
			</div>
			<div id="bottom-panel" class="fixed-width">
				<a href="index.php">
					<img alt="" src="img/logo.png">
				</a>
				<nav>
					<ul>
						<li><a href="/">Home</a></li>
						<li><a href="#">Features</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Help</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<div class="section-content">
			<h1>The Perfect Online Calendar!</h1>
			<h4>Stuffed with all the features you'll ever need, without a costly subscription attached.</h4>
		</div>
		<div class="section-content">
			<div class="content-box">
				<h4>Shared Calendars</h4>
				<p>Share your calendar with friends, family, and coworkers. Promote collaboration and boost productivity by allowing multi-user editing.</p>
			</div>
			<div class="content-box">
				<h4>Embeddable</h4>
				<p>Share your calendar with the world by posting it to a website. Choose from a selection of embeddable widgets, including an upcoming events list and a mini calendar.</p>
			</div>
		</div>
		<div id="footer">
			<p class="fixed-width">Copyright &copy; 2014 Nicholas Chapin. All rights reserved. <a href="https://bitbucket.org/nmchapin/cis-355-project">View Source Code on BitBucket.</a></p>
		</div>
	</body>
</html>