<?php
/**
 * Displays the "View Event" page
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

// Initialize the web application
include( 'includes/core.php' );
include( 'includes/func-calendar.php' );

ob_start();
session_start();

// Application error messages
$ApplicationErrors = array();

// If the user attempting to access this page is not logged in,
// then redirect him or her to the login page.
if ( ! isset( $_SESSION['user_id'] ) )
	exit( header( 'Location: login.php' ) );

if ( isset( $_GET['eventID'] ) ) {

	// The user send a delete request for an event
	if ( isset( $_POST['delete_event'] ) ) {

		// Attempt to delete the event. If there is a problem, display an error message.
		// Otherwise, redirect the user to the calendar/events view page.
		if ( ! delete_event( intval( $_GET['eventID'] ), true ) )
			array_push( $ApplicationErrors, 'There was a problem deleting the event.' );
		else
			exit( header( 'Location: calendar.php' ) );
	}

	// Retrieve the event details
	if ( ! $event_details = retrieve_event( intval( $_GET['eventID'] ) , $_SESSION['user_id'] ) )
		array_push( $ApplicationErrors, 'There was a problem retrieving the requested event.' );

}
else
	exit( header( 'Location: calendar.php' ) );

?>
<!DOCTYPE html>
<html lang="en" class="default">
	<head>
		<meta charset="UTF-8">
		<title>Calendr - View Event</title>
		<meta name="description" content="Calendr is a free, easy-to-use event management system designed to help you keep track of life's important events.">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Style Sheets -->
		<link rel="stylesheet" href="css/style-backend.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="topbar" class="clearfix">
			<a href="/calendar.php">
				<img src="/img/logo-small.png" alt="" width="118" height="28">
			</a>
			<ul id="toolbar"><li><a href="#">&nbsp;</a></li></ul>
			<ul id="account">
				<li>
					<a href="#"><?php echo $_SESSION['user_name']; ?></a>
				</li>
			</ul>
		</div>
		<div id="content">
			<?php

			// Output all application-related error messages to the screen
			if ( isset( $ApplicationErrors ) && count( $ApplicationErrors ) > 0 ) {
				echo	'<div class="error-banner">
							<div>
								<p>The following errors occurred:</p>
								<ul>';
				
				foreach ( $ApplicationErrors as $Error )
					echo '<li>' . $Error . '</li>';
				
				echo 			'</ul>
						</div>
					</div>';
			}

			?>
			<div class="page-heading">View Event</div>
			<form>
				<ul>
					<li>
						<label>Name</label>
						<span class="read-only-field"><?php echo (isset( $event_details['name'] ) ) ? $event_details['name'] : '&nbsp;'; ?></span>
					</li>
					<li><label>&nbsp;</label><hr></li>
					<li>
						<label>Date</label>
						<span class="read-only-field"><?php echo ( isset( $event_details['date'] ) ) ? date( 'F j, Y', strtotime( $event_details['date'] ) ) : '&nbsp;'; ?></span>
					</li>
					<li><label>&nbsp;</label><hr></li>
					<li>
						<label>Time</label>
						<span class="read-only-field">
						<?php
							if ( isset( $event_details['start_time'] ) && $event_details['all_day'] != 1 ) {
								if ( isset( $event_details['start_time'] ) && isset( $event_details['end_time'] ) )
									echo date( 'g:i A', strtotime( $event_details['start_time'] ) ) . ' &#8211; ' . date( 'g:i A', strtotime( $event_details['end_time'] ) );
							} else {
								echo 'All Day';
							}
						?>
						</span>
					</li>
					<li><label>&nbsp;</label><hr></li>
					<?php if ( isset( $event_details['description'] ) && $event_details['description'] != '' ): ?>
					<li>
						<label>Description</label>
						<span class="read-only-field"><?php echo $event_details['description']; ?></span>
					</li>
					<li><label>&nbsp;</label><hr></li>
					<?php endif; ?>
					<li>
						<label>Calendar</label>
						<span class="read-only-field">
						<?php

						// If the event was successfully retrieved from the database, then calendar_id should contain an integer.
						// Otherwise, we assume something went wrong.
						if ( isset( $event_details['calendar_id'] ) ) {
							// Use the get_calendar() function to retrieve the calendar name.
							if ( $calendar = get_calendar( intval( $event_details['calendar_id'] ), $_SESSION['user_id'] ) )
								echo $calendar[0];
							else
								echo $event_details['calendar_id'];
						} else
							echo '&nbsp;';

						?>
						</span>
					</li>
				</ul>
				<?php
				// If no application-related errors occurred, allow the user to edit or delete the event
				if ( count( $ApplicationErrors ) == 0  ) : ?>
				<a href="/<?php echo ( isset( $_GET['eventID'] ) ) ? 'edit-event.php?eventID=' . intval( $_GET['eventID'] ) : 'calendar.php'; ?>" class="button button-primary">Modify Event</a>
				<a href="#delete-prompt" class="button button-primary">Delete Event</a>
				<?php endif; ?>
				<a href="/calendar.php" class="button button-secondary">Go Back</a>
			</form>
		</div>
		<div id="delete-prompt" class="lightbox-background">
			<div class="lightbox-dialog">
				<h2>Delete Event</h2>
				<div class="message">Are you sure you want to delete this event?</div>
				<form method="post" class="tasklist" action="/view-event.php<?php if ( isset( $_GET['eventID'] ) ) echo '?eventID=' . intval( $_GET['eventID'] ); ?>">
					<input type="submit" name="delete_event" class="button button-secondary" value="Yes" />
					<a class="button button-secondary" href="#">No</a>
				</form>
			</div>
		</div>
	</body>
</html>