<?php
/**
 * Displays the calendar page
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

// Initialize the web application
include( 'includes/core.php' );
include( 'includes/func-calendar.php' );

ob_start();
session_start();

// If the user attempting to access this page is not logged in,
// then redirect them to the login page.
if ( ! isset( $_SESSION['user_id'] ) )
	header('Location: login.php');

// Retrieve the current user's calendar(s)
$UserCalendarList = get_user_calendars( $_SESSION['user_id'] );

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Calendr</title>
		<meta name="description" content="Calendr is a free, easy-to-use event management system designed to help you keep track of life's important events.">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Style Sheets -->
		<link rel="stylesheet" href="css/style-backend.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
		<!-- Scripts -->
		<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
		<script src="/js/func-calendar.js"></script>
	</head>
	<body>
		<div id="topbar" class="clearfix">
			<a href="/calendar.php">
				<img src="/img/logo-small.png" alt="" width="118" height="28" />
			</a>
			<ul id="toolbar">
				<?php 
				// If the current user has at least one calendar, then show the "New Event" toolbar button.
				// Otherwise, do not display the button.
				if ( ! empty( $UserCalendarList ) ) : ?>
				<li>
					<a href="/new-event.php">New Event</a>
				</li>
				<?php endif; ?>
				<li>
					<a href="/new-calendar.php">New Calendar</a>
				</li>
			</ul>
			<ul id="account">
				<li>
					<a href="#"><?php echo $_SESSION['user_name']; ?></a>
				</li>
			</ul>
		</div>
		<div id="content">
			<div id="sidebar">
					<h3>Personal Calendars</h3>
					<?php 

					// If the current user does not have any calendars, display a short message
					// explaining how a new calendar can be created. Otherwise, output a list of
					// all the user's calendars.
					if ( empty( $UserCalendarList ) )
						echo '<p>Click the "New Calendar" button above to create your first calendar.</p>';
					else
						print_user_calendar_list( $UserCalendarList );

					?>
			</div>
			<div id="calendar-view">
				<noscript>
					<div class="error-banner">
						<div> 
							<p><strong>JavaScript is required to display calendar events.</strong></p>
						</div>
					</div>
				</noscript>
				<?php

				$month = NULL;
				$year = NULL;

				// If the user clicked on the backward or forward navigational buttons on the calendar
				// head, then their browser will have sent a month and year value to the server. These
				// two numbers represent the particular month and year that the user wishes to view.
				if ( isset( $_GET['month'] ) && isset( $_GET['year'] ) ) {

					$tmpMonth = intval( $_GET['month'] );
					$tmpYear = intval( $_GET['year'] );

					// Make sure the user did not send bogus month and year values to the server
					if ( $tmpMonth >= 1 && $tmpMonth <= 12 )
						$month = $tmpMonth;
					if ( $tmpYear >= 2014 && $tmpYear <= 2050 )
						$year = $tmpYear;
				}

				// Output the HTML code for the monthly calendar
				print_monthly_calendar( $month, $year );

				?>
			</div>
		</div>
	</body>
</html>