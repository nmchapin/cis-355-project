<?php
/**
 * Displays the registration page for new users
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

// Initialize the web application
include( 'includes/core.php' );
include( 'includes/func-user.php' );

// Application error messages
$ApplicationErrors = array();

// Check if the user pressed the submit button
if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

	$FieldInvalid = array();
	$FormErrors = array();

	// Make sure the text field for 'First Name' contains only one word
	if( ! preg_match('/^[A-Za-záéíóúñü]+$/', $_POST['first_name'] ) ) {
		$FieldInvalid['first_name'] = true;
		array_push( $FormErrors, 'You forgot to enter your first name.' );
	}

	// Make sure the text field for 'Last Name' contains only one word
	if( ! preg_match('/^[A-Za-záéíóúñü]+$/', $_POST['last_name'] ) ) {
		$FieldInvalid['last_name'] = true;
		array_push( $FormErrors, 'You forgot to enter your last name.' );
	}

	// Make sure the email address is valid
	if( filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL) == false ) {
		$FieldInvalid['email'] = true;
		array_push( $FormErrors, 'You have entered an invalid email address.' );
	}

	// Make sure the email address isn't already registered
	if ( user_registered( $_POST['email'] ) ) {
		$FieldInvalid['email'] = true;
		array_push( $FormErrors, 'A user account already exists with the email <b>' . $_POST['email'] . '</b>' );
	}

	// Make sure the password the user entered match and are six characters in length
	if ( isset( $_POST['password1'] ) && isset( $_POST['password2'] )  ) {

		if ( strlen( $_POST['password1'] ) < 6 || strlen( $_POST['password2'] ) < 6 ) {
			$FieldInvalid['password1'] = true;
			array_push( $FormErrors, 'Passwords must be at least six characters in length' );
		}

		if( $_POST['password1'] != $_POST['password2'] ) {
			$FieldInvalid['password1'] = true;
			$FieldInvalid['password2'] = true;
			array_push( $FormErrors, 'Passwords do not match' );
		}

	} else {
		$FieldInvalid['password1'] = true;
		$FieldInvalid['password2'] = true;
	}

	// Format names and email
	$_POST['first_name'] = ucwords( strtolower( $_POST['first_name'] ) );
	$_POST['last_name'] = ucwords( strtolower( $_POST['last_name'] ) );
	$_POST['email'] = strtolower( $_POST['email'] );

	// If there are no errors and everything the user entered is valid, then create an account for them
	if ( count( $FieldInvalid ) == 0 && count( $FormErrors ) == 0 ) {

		if ( create_user( $_POST['first_name'], $_POST['last_name'], $_POST['email'], $_POST['password1'] ) ) {
			
			exit( header( 'Location: login.php' ) );

		} else {

			array_push(
				$ApplicationErrors, 
				'There was a problem creating the user account.Please try again at a later time. If the problem persists, please contact the webmaster.'
			);
		}
	}
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Calendr - Free Online Calendar</title>
		<meta name="description" content="Calendr is a free, easy-to-use event management system designed to help you keep track of life's important events.">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Style Sheets -->
		<link rel="stylesheet" href="css/style.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<h1 id="registration-title">Get Started with Calendr</h1>
			<?php

			// Output all application-related error messages to the screen
			if ( isset( $ApplicationErrors ) && count( $ApplicationErrors ) > 0 ) {
				echo	'<div class="nofc nofc-error form-message">
							<div>
								<p>The following errors occurred:</p>
								<ul>';
				
				foreach ( $ApplicationErrors as $Error )
					echo '<li>' . $Error . '</li>';
				
				echo 		'</ul>
						</div>
					</div>';
			}

			// Output all error messages to the screen
			if ( isset( $FormErrors ) && count( $FormErrors ) > 0 ) {
				echo	'<div class="nofc nofc-error form-message">
							<div>
								<p>Please correct the following issues:</p>
								<ul>';
				
				foreach ( $FormErrors as $Error )
					echo '<li>' . $Error . '</li>';
				
				echo 		'</ul>
						</div>
					</div>';
			}

			?>
		<form method="post" id="register" action="/register.php">
			<ul>
				<li>
					<label for="first_name">First Name</label>
					<input type="text" name="first_name" autocomplete="off"
					<?php if ( isset( $FieldInvalid['first_name'] ) ) echo 'class="error"'; ?>
					<?php if ( isset( $_POST['first_name'] ) ) echo 'value="' . $_POST['first_name'] . '"'; ?> />
				</li>
				<li>
					<label for="last_name">Last Name</label>
					<input type="text" name="last_name" autocomplete="off"
					<?php if ( isset( $FieldInvalid['last_name'] ) ) echo 'class="error"'; ?>
					<?php if ( isset( $_POST['last_name'] ) ) echo 'value="' . $_POST['last_name'] . '"'; ?> />
				</li>
				<li>
					<label for="email">E-mail Address</label>
					<input type="email" name="email"
					<?php if ( isset( $FieldInvalid['email'] ) ) echo 'class="error"'; ?>
					<?php if ( isset( $_POST['email'] ) ) echo 'value="' . $_POST['email'] . '"'; ?> />
				</li>
				<li>
					<label for="password1">Create Password</label>
					<input type="password" name="password1"
					<?php if ( isset( $FieldInvalid['password1'] ) ) echo 'class="error"'; ?>
					<?php if ( isset( $_POST['password1'] ) ) echo 'value="' . $_POST['password1'] . '"'; ?> />
				</li>
				<li>
					<label for="password2">Confirm Password</label>
					<input type="password" name="password2"
					<?php if ( isset( $FieldInvalid['password2'] ) ) echo 'class="error"'; ?>
					<?php if ( isset( $_POST['password2'] ) ) echo 'value="' . $_POST['password2'] . '"'; ?> />
				</li>
				<li>
					<input type="submit" class="button button-primary" value="Register" />
					<a href="/index.php" class="button button-secondary">Back to Home</a>
				</li>
			</ul>
		</form>
	</body>
</html>