<?php
/**
 * Displays the login form for existing users
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

ob_start();
session_start();

// Initialize the web application
include( 'includes/core.php' );
include( 'includes/func-user.php' );

// Check if the user pressed the submit button
if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

	$FieldInvalid = array();
	$FormErrors = array();

	// Make sure the email address is valid
	if ( filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL) == false ) {
		$FieldInvalid['email'] = true;
		array_push( $FormErrors, 'You have entered an invalid email address.' );

	} else {

		// Check if the user's credentials are valid
		if ( user_credentials_valid( $_POST['email'], $_POST['password'] ) ) {

			// Create a new PHP session so that the user can stay logged in
			session_regenerate_id();

			$user = get_user( $_POST['email'] );
			$_SESSION['user_id'] =  $user['ID'];
			$_SESSION['user_name'] = $user['first_name'] . ' ' . $user['last_name'];

			session_write_close();
			exit( header( 'Location: calendar.php' ) );

		} else {
			array_push( $FormErrors, 'The username or password is invalid.' );
		}
	}
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Calendr - Free Online Calendar</title>
		<meta name="description" content="Calendr is a free, easy-to-use event management system designed to help you keep track of life's important events.">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Style Sheets -->
		<link rel="stylesheet" href="css/style.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<h1 id="registration-title">Calendr Login</h1>
		<div class="nofc nofc-message form-message">
			<div>
					<p>You may login with the following credentials:</p>
					<ul>
						<li>Email: <b>demo@user.dev</b></b></li>
						<li>Password: <b>password</b></li>
					</ul>
			</div>
		</div>
		<?php

			// Output all error messages to the screen
			if ( isset( $FormErrors ) && count( $FormErrors ) > 0 ) {
				echo	'<div class="nofc nofc-error form-message">
							<div>
								<p>Please correct the following issues:</p>
								<ul>';
				
				foreach ( $FormErrors as $Error )
					echo '<li>' . $Error . '</li>';
				
				echo 			'</ul>
						</div>
					</div>';
			}

			?>
		<form method="post" id="register" action="/login.php">
			<ul>
				<li>
					<label for="email">Email</label>
					<input type="text" name="email"
					<?php if ( isset( $FieldInvalid['email'] ) ) echo 'class="error"'; ?>
					<?php if ( isset( $_POST['email'] ) ) echo 'value="' . $_POST['email'] . '"'; ?>>
				</li>
				<li>
					<label for="password">Password</label>
					<input type="password" name="password"
					<?php if ( isset( $FieldInvalid['password'] ) ) echo 'class="error"'; ?>
					<?php if ( isset( $_POST['password'] ) ) echo 'value="' . $_POST['password'] . '"'; ?>>
				</li>
				<li>
					<input type="submit" class="button button-primary" value="Sign In" />
				</li>
			</ul>
		</form>
	</body>
</html>