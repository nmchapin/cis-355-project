<?php
/**
 * This file handles AJAX requests
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

// Initialize the web application
include( 'core.php' );
include( 'func-calendar.php' );

ob_start();
session_start();

// Check if this page recieved a POST request
if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

	// Perform the requested function
	switch ( $_POST['function'] ) {
		case 'delete_calendar':
			echo delete_calendar( intval( $_POST['calendar_id'] ) ) ? 1 : 0;
			break;
		case 'retrieve_events':
			$events = retrieve_events( $_SESSION['user_id'], intval( $_POST['month'] ), intval( $_POST['year'] ) );
			if ( $events === 2 )
				echo 2;
			else if ( $events === false ) 
				echo 0;
			else
				echo json_encode( $events );
			break;
	}

}

?>