<?php
/**
 * Core file for configuring and initializing the SEMS environment
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

// Define absolute path to this file's directory
define( 'ABSPATH', dirname(__FILE__) . '/' );

// Grab the configuration settings
require_once( ABSPATH . 'config.php' );

// Connect to the database
$db = new mysqli( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME );

// Check if the connection was successful
if ( $db->connect_errno )
	die( 'Unable to connect to database [' . $db->connect_error . ']' );

// Set the default time zone
date_default_timezone_set( 'America/Detroit' );

// Include the functions file
require_once( ABSPATH . 'functions.php' );

// Check if the database tables are built. If no result set is returned, we know that
// this is the first time the application has run. Therefore, we need to build the tables.
if ( $db->query( "SHOW TABLES IN DB_NAME" ) === FALSE )
	build_tables( $db );

?>