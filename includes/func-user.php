<?php
/**
 * Contains user account-related functions
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

/**
 * Checks whether a particular email address is associated with a user account
 * @param {String} email - The email addres to search for
 * @return {Boolean} - Returns TRUE if a user account exists with the specified email
 *                     address. Otherwise, returns false.
 */
function user_registered( $email ) {

	global $db;

	if( $result = $db->query( "SELECT email FROM user WHERE email = '$email'" ) ) {
		if ( $result = $result->fetch_row() ) {
			if( $result[0] == $email )
				return true;
			else
				return false;
		}

	}
}

/**
 * Adds a user to the database
 * @param {String} first_name - The first name of the user
 * @param {String} last_name - The last name of the user
 * @param {String} email - The user's email address
 * @param {String} password - The user's desired account password
 * @return {Boolean} - Returns TRUE if a user account was created successfully.
 *                     Otherwise, returns false.
 */
function create_user( $first_name, $last_name, $email, $password ) {

	global $db;

	if ( $stmt = $db->prepare( "INSERT INTO user (first_name, last_name, email, password, date_registered) VALUES (?, ?, ?, ?, ?)" ) ) {

		// Hash password and set value of registration date
		$date_registered = date( 'Y-m-d H:i:s' );
		$password = md5( $password );

		$stmt->bind_param( 'sssss', $first_name, $last_name, $email, $password, $date_registered );
		$stmt->execute();
		$stmt->close();

		return true;
	}

	return false;
}

/**
 * Verifies whether a particular password string matches the
 * password of an account.
 * @param {String} email - A user's email address
 * @param {String} password - A password string
 * @return {Boolean} - Returns TRUE if the password is matches the account
 *                     password. Otherwise, returns false.
 */
function user_credentials_valid( $email, $password ) {

	global $db;

	if ( $stmt = $db->prepare( "SELECT COUNT(*) FROM user WHERE email = ? AND password = ?" ) ) {

		$hashed_password = md5( $password );

		$stmt->bind_param( 'ss', $email, $hashed_password );
		$stmt->execute();
		$stmt->bind_result( $row_count );

		if ( $stmt->fetch() && $row_count == 1 ) {
			$stmt->close();
			return true;
		}

		$stmt->close();

	}

	return false;

}

/**
 * Returns information about a particular user
 * @param {String} email - A user's email address
 * @return {Boolean} - Returns an associative array containing
 *                     user account information. On error, returns
 *                     -1.
 */
function get_user( $email ) {

	global $db;

	if ( $stmt = $db->prepare( "SELECT ID, first_name, last_name, email, date_registered FROM user WHERE email = ?" ) ) {

		$stmt->bind_param( 's', $email );
		$stmt->execute();
		$stmt->bind_result( $ID, $first_name, $last_name, $email, $date_registered );

		if ( $stmt->fetch() ) {
			$output = array(
				'ID' => $ID,
				'first_name' => $first_name,
				'last_name' => $last_name,
				'email' => $email,
				'date_registered' => $date_registered
			);
			$stmt->close();
			return $output;
		}

		$stmt->close();

	}

	return false;

}

?>