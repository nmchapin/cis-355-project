<?php
/**
 * Contains secret key and configuration settings for MySQL
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

/* ---------------- Secret Key ---------------- */

define( 'SALT', '35`6ul+}3t}IIV4hTe.T*R(Jxw(K5A,U-?xSp_TvYHCEYT/p/j(JfH6ejwhnzPBK' );

/* -------------- MySQL Settings -------------- */
// The MySQL hostname
define( 'DB_HOST', 'localhost' );

// The database name
define( 'DB_NAME', 'nickchap_calendr' );

// The database username
define( 'DB_USER', 'nickchap_admin' );

// The database password
define( 'DB_PASSWORD', 'fa3Etrec' );