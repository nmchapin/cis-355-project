<?php
/**
 * Contains calendar-related functions
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

/**
 * Prints out a monthly calendar view for a particular month and year
 * @param {Integer} month - A month number
 * @param {Integer} year - A year number
 */
function print_monthly_calendar( $month = NULL, $year = NULL ) {

	// Assign a default value to each unset parameter
	if ( is_null( $month ) )
		$month = intval( date( 'm' ) );
	if ( is_null( $year ) )
		$year = intval( date( 'Y' ) );


	// The number of days in a month for a given year
	$NbrOfDaysInMonth = intval( cal_days_in_month( CAL_GREGORIAN, $month, $year ) );
	
	// The first weekday of a month for a given year
	// 0 = Sunday, 1 = Monday, ..., 6 = Saturday
	$FirstDayOfMonth = intval( date( 'w', mktime( 0, 0, 0, $month, 1, $year ) ) );

	?>
	<div id="calendar">
		<div id="calendar-title">
			<?php

				// Set link for the previous month button
				if ( $month > 1 )
					echo "<a id='prev' class='nav' href='calendar.php?month=" . ( $month - 1 ) . "&year=" . $year . "'>&#9666;</a>";
				else
					echo "<a id='prev' class='nav' href='calendar.php?month=12&year=" . ( $year - 1 ) . "'>&#9666;</a>";


				// Output the month and year
				echo date( 'F', mktime( 0, 0, 0, $month, 10 ) ) . ' ' . $year;

				// Set link for the next month button
				if( $month < 12 )
					echo "<a id='next' class='nav' href='calendar.php?month=" . ( $month + 1 ) . "&year=" . $year . "'>&#9656;</a>";
				else
					echo "<a id='next' class='nav' href='calendar.php?month=1&year=" . ( $year + 1 ) . "'>&#9656;</a>";

				// Counts the number of days that have been outputted
				$DayCount = 1;

			?>
		</div>
		<div id="calendar-grid">
			<div id="calendar-days">
				<div class="day-name"><span>Sun</span></div>
				<div class="day-name"><span>Mon</span></div>
				<div class="day-name"><span>Tue</span></div>
				<div class="day-name"><span>Wed</span></div>
				<div class="day-name"><span>Thu</span></div>
				<div class="day-name"><span>Fri</span></div>
				<div class="day-name"><span>Sat</span></div>
			</div>
			<div class="week-days">
			<?php

				// Counts the number of weekdays that have been outputted
				$WeekdaysOutputted = 0;

				// Output empty cells for each weekday before the first day of the month
				for ( $i = 0; $i < $FirstDayOfMonth; $i++ ) {

					echo '<div class="week-day empty-day">&nbsp;</div>';
					$WeekdaysOutputted++;

				}

				// Output the first day of the month
				echo '<div class="week-day"><span class="day-number first-day">1</span></div>';
				$WeekdaysOutputted++;

				// Contains the next calendar day to be outputted
				$CalendarDay = 2;

				// Output a cell for each day after the first day in the first week of the month
				for ( $WeekdaysOutputted; $WeekdaysOutputted < 7; $WeekdaysOutputted++ ) {
					echo "<div class='week-day'><span class='day-number'>$CalendarDay</span></div>";
					$CalendarDay++;
				}

			?>
			</div>
			<?php

				// Output the remaining calendar days of the month
				while ( $CalendarDay <= $NbrOfDaysInMonth ) {

					// Output a week
					for ( $i = 0; $i < 7; $i++ ) {

						// If this is the first iteration, then we need output HTML code
						// to start a new calendar week.
						if ( $i == 0 )
							echo '<div class="week-days">';
						// If the next calendar day is not greater than the total number of days in
						// the current month, then output it.
						if ( $CalendarDay <= $NbrOfDaysInMonth ) {
							echo "<div class='week-day'><span class='day-number'>$CalendarDay</span></div>";
							$CalendarDay++;
						}
						else {
							echo '<div class="week-day empty-day">&nbsp;</div>';
						}
						// If this is the sixth iteration, then we have just outputted the last day of
						// the week. We need to output the HTML closing tag in order to end the current week.
						if ( $i == 6 )
							echo '</div>';

					}
				}
			?>
		</div>
	</div>
<?php
}

/**
 * Retrieves all of the calendars owned by a particular user
 * @param {Integer} UserID - The ID number of the user
 * @return {Array|Boolean} - Returns a numeric array of calendars
 */
function get_user_calendars( $UserID ) {
	
	global $db;

	$UserCalendarList = array();

	if ( $stmt = $db->prepare( "SELECT ID, name, color, description FROM calendar WHERE user_id = ? ORDER BY name" ) ) {

		$stmt->bind_param( 'i', $UserID );
		$stmt->execute();
		$stmt->bind_result( $id, $name, $color, $description );

		$calendar_list = array();

		if ( $stmt->fetch() ) {

			do
				array_push( $UserCalendarList, array( $id, $name, $color, $description ) );
			while ( $stmt->fetch() );

		}

		$stmt->close();

		return $UserCalendarList;

	}
}

/**
 * Returns a numeric array of calendar ID numbers owned by a particular user
 * @param {Integer} CalendarID - The ID number of the calendar
 * @param {Integer} UserID - The ID number of the user
 * @return {Array|Boolean} - Returns a numeric array of calendar ID numbers. If the
 *                           user does not have any calendars, FALSE is returned.
 */
function get_user_calendar_ids( $UserID ) {

	$UserCalendarList = get_user_calendars( $UserID );
	$UserCalendarIDs = array();

	foreach ( $UserCalendarList as $Calendar ) {
		array_push( $UserCalendarIDs, $Calendar[0] );
	}
	
	return count( $UserCalendarIDs ) > 0 ? $UserCalendarIDs : false;

}

/**
 * Determines whether a particular calendar is owned by an particular user
 * @param {Integer} CalendarID - The ID number of the calendar
 * @param {Integer} UserID - The ID number of the user
 * @return {Boolean} - Returns TRUE if a calendar is owned by a particular user, FALSE otherwise.
 */
function is_calendar_owner( $CalendarID, $UserID ) {

	$UserCalendars = get_user_calendars( $UserID );

	for ( $index = 0; $index < count( $UserCalendars ); $index++) {
		if ( $UserCalendars[$index][0] === $CalendarID ) {
			return true;
		}
	}

	return false;

}

/**
 * Creates a HTML list containing a user's calendar(s) and then outputs it to the screen
 * @param {Array} UserCalendarList - A numeric array containing a user's calendars and their
 *                                   associated attributes/details
 */
function print_user_calendar_list( $UserCalendarList ) {

		echo '<ul>';
		foreach ( $UserCalendarList as $Calendar ) {
			echo	'<li id="cal-id-' . $Calendar[0] . '" class="cal-item">
						<span class="cal-color cal-color-' . $Calendar[2] . '">&nbsp;</span>
						<span class="cal-name" title="'. $Calendar[3] .'">' . $Calendar[1] . '</span>
						<ul class="cal-dropdown">
							<li>&#9662;</li>
						</ul>
					</li>';
		}
		echo '</ul>';
}

/**
 * Creates a new calendar entry in the database
 * @param {String} name - (META DATA) The name of the calendar
 * @param {String} description - (META DATA) A description of the calendar
 * @param {Integer} color - (META DATA) The color ID of the calendar
 * @param {Integer} user_id - The ID number of the user/owner of the calendar
 * @return {Boolean} - Returns TRUE on success, FALSE otherwise
 */
function create_calendar( $name, $description, $color, $user_id ) {

	global $db;

	if ( $stmt = $db->prepare( "INSERT INTO calendar VALUES(NULL, ?, ?, ?, ?)" ) ) {

		$stmt->bind_param( 'issi', $user_id, $name, $description, $color );
		$stmt->execute();
		
		$return_value = $stmt->affected_rows === 1 ? true : false;

		$stmt->close();

		return $return_value;

	}

	return false;
}

/**
 * Updates a calendar in the database
 * @param {String} name - (META DATA) The name of the calendar
 * @param {String} description - (META DATA) A description of the calendar
 * @param {Integer} color - (META DATA) The color ID of the calendar
 * @param {Integer} calendar_id - The ID number of the calendar to update
 * @return {Boolean} - Returns TRUE on success, FALSE otherwise
 */
function update_calendar( $name, $description, $color, $calendar_id ) {

	global $db;

	if ( $stmt = $db->prepare( "UPDATE calendar SET name = ?, description = ?, color = ? WHERE ID = ?" ) ) {

		$stmt->bind_param( 'ssii', $name, $description, $color, $calendar_id );
		$stmt->execute();

		$return_value = mysql_errno() == 0 ? true : false;

		$stmt->close();

		return $return_value;

	}

	return false;
}

/**
 * Deletes a calendar from the database
 * @param {Integer} calendar_id - The ID number of the calendar to delete
 * @param {Boolean} VerifyOwner - Determines whether the function will
 *                                allow a user delete an event. If the
 *                                requester is the event owner, then
 *                                there are no security issues. However,
 *                                this function will allow non-event
 *                                owners to request the deletion of an
 *                                event if this flag is set to FALSE.
 * @return {Boolean} - Returns TRUE on success, FALSE otherwise
 */
function delete_calendar( $calendar_id, $verify_owner = true ) {

	global $db;

 	if ( $verify_owner ) {

		$owner_id = ( isset( $_SESSION['user_id'] ) ? $_SESSION['user_id'] : NULL );

		if ( $stmt = $db->prepare( "DELETE FROM calendar WHERE ID = ? AND user_id = ?" ) ) {

			$stmt->bind_param( 'ii', $calendar_id, $owner_id );
			$stmt->execute();

			$return_value = $stmt->affected_rows > 0 ? true : false;

			$stmt->close();

			return $return_value;

		}

	} else {

		if ( $stmt = $db->prepare( "DELETE FROM calendar WHERE ID = ?" ) ) {

			$stmt->bind_param( 'i', $calendar_id );
			$stmt->execute();

			$return_value = $stmt->affected_rows > 0 ? true : false;

			$stmt->close();

			return $return_value;

		}

	}

	return false;
}

/**
 * Retrieves a calendar from the database
 * @param {Integer} calendar_id - The ID number of the calendar to retrieve
 * @param {Integer} owner_id - The ID number of the user/owner of the calendar
 * @return {Array|Boolean} - Returns a numeric array containing containing a calendar's
 *                           name, description, and color. On error, returns FALSE.
 */
function get_calendar( $calendar_id, $owner_id ) {

	global $db;

	if ( $stmt = $db->prepare( "SELECT name, description, color FROM calendar WHERE ID = ? AND user_id = ? LIMIT 1" ) ) {

		$stmt->bind_param( 'ii', $calendar_id, $owner_id );
		$stmt->execute();

		$stmt->bind_result( $name, $description, $color );

		if ( $calendar = $stmt->fetch() ) {
			$stmt->close();
			return array( $name, $description, $color );
		}

		$stmt->close();

	}

	return false;
}

/**
 * Checks whether a given date string is valid. It must be in the format MM/DD/YYYY
 * @param {String} time - A date string
 * @return {Boolean} - Returns TRUE if valid, FALSE otherwise
 */
function is_date_valid( $date ) {

	return preg_match( '/(?:0[1-9]|10|11|12)[\/-](?:0?[1-9]|[12]\d|3[01])[\/-][0-9]{4}/', $date );  

}

/**
 * Checks whether a given time string is valid. It must be in the format HH:MM AM|PM
 * @param {String} time - A time string
 * @return {Boolean} - Returns TRUE if valid, FALSE otherwise
 */
function is_time_valid( $time ) {

	return preg_match( '/(?:1[0-2]|0?[0-9]):(?:[0-5][0-9]) ?(?:am|AM|pm|PM)/', $time );

}

/**
 * Inserts an event into the database
 * @param {Integer} UserID - A numeric array containing the event details (look at diagram below)
 * @return {Boolean} - Returns TRUE on success, FALSE otherwise
 */
function insert_event( $event ) {
	/* ---- {array} Event ----
	 * [0]  -  name
	 * [1]  -  date
	 * [2]  -  start time
	 * [3]  -  end time
	 * [4]  -  description
	 * [5]  -  all day flag
	 * [6]  -  calendar ID
	*/

	global $db;

	if ( $stmt = $db->prepare( "INSERT INTO event VALUES(NULL, ?, ?, ?, ?, ?, ?, ?)" ) ) {

		$stmt->bind_param( 'isssssi', $event[6], $event[0], $event[1], $event[2], $event[3], $event[4], $event[5] );
		$stmt->execute();

		$return_value = $stmt->affected_rows === 1 ? true : false;

		$stmt->close();

		return $return_value;

	}

	return false;

}

/**
 * Updates an event in the database
 * @param {Integer} EventID - The ID number of the event
 * @param {Integer} UserID - A numeric array containing the event details (look at diagram below)
 * @return {Boolean} - Returns TRUE on success, FALSE otherwise
 */
function update_event( $EventID, $Event ) {
	/* ---- {array} Event ----
	 * [0]  -  name
	 * [1]  -  date
	 * [2]  -  start time
	 * [3]  -  end time
	 * [4]  -  description
	 * [5]  -  all day flag
	 * [6]  -  calendar ID
	*/

	global $db;

	if ( $stmt = $db->prepare( "UPDATE event SET calendar_id = ?, name = ?, `date` = ?, start_time = ?, end_time = ?, description = ?, all_day = ? WHERE ID = ?" ) ) {

		$stmt->bind_param( 'issssssi', $Event[6], $Event[0], $Event[1], $Event[2], $Event[3], $Event[4], $Event[5], $EventID );
		$stmt->execute();

		$return_value = mysql_errno() == 0 ? true : false;

		$stmt->close();

		return $return_value;

	}

	return false;

}

/**
 * Deletes an event from the database
 * @param {Integer} EventID - The ID number of the event
 * @param {Boolean} VerifyOwner - Determines whether the function will
 *                                allow a user delete an event. If the
 *                                requester is the event owner, then
 *                                there are no security issues. However,
 *                                this function will allow non-event
 *                                owners to request the deletion of an
 *                                event if this flag is set to FALSE.
 * @return {Boolean} - Returns TRUE on success, FALSE otherwise
 */
function delete_event( $EventID, $VerifyOwner = true ) {

	/* Retrieve the user's calendar ID numbers and put them
	   into a comma separated list. If the user does not have
	   any calendars, then they will not have any events
	   stored in the database. Return status code 2 which
	   symbolizes "No Events Found".
	*/
	if ( $UserCalendarIDs = get_user_calendar_ids( $_SESSION['user_id'] ) )
		$UserCalendarIDs = implode( ', ', $UserCalendarIDs );
	else
		return false;

	global $db;

 	if ( $VerifyOwner ) {

		$owner_id = ( isset( $_SESSION['user_id'] ) ? $_SESSION['user_id'] : NULL );

		if ( $stmt = $db->prepare( "DELETE FROM event WHERE ID = ? AND calendar_id IN ($UserCalendarIDs)" ) ) {

			$stmt->bind_param( 'i', $EventID );
			$stmt->execute();

			$return_value = $stmt->affected_rows > 0 ? true : false;

			$stmt->close();

			return $return_value;

		}

	} else {

		if ( $stmt = $db->prepare( "DELETE FROM event WHERE ID = ? AND calendar_id IN ($UserCalendarIDs)" ) ) {

			$stmt->bind_param( 'i', $event_id );
			$stmt->execute();

			$return_value = $stmt->affected_rows > 0 ? true : false;

			$stmt->close();

			return $return_value;

		}

	}

	return false;

}

/**
 * Retrieves an event from the database
 * @param {Integer} EventID - The ID number of the event
 * @param {Integer} UserID - The ID number of the user
 * @return {Array|Boolean} - Returns an associative array containing the event attributes.
 *                           On error, boolean FALSE is returned.
 */
function retrieve_event( $EventID, $UserID ) {

	/* Retrieve the user's calendar ID numbers and put them
	   into a comma separated list. If the user does not have
	   any calendars, then they will not have any events
	   stored in the database. Return status code 2 which
	   symbolizes "No Events Found".
	*/
	if ( $UserCalendarIDs = get_user_calendar_ids( $UserID ) )
		$UserCalendarIDs = implode( ', ', $UserCalendarIDs );
	else
		return false;

	global $db;

	if ( $stmt = $db->query( "SELECT * FROM event WHERE ID = $EventID AND calendar_id IN ($UserCalendarIDs)" ) ) {

		if ( $stmt->num_rows === 1 ) {
			if ( $event = $stmt->fetch_row() ) {
				$stmt->close();
				return array(
					'ID' => $event[0],
					'calendar_id' => $event[1],
					'name' => $event[2],
					'date' => $event[3],
					'start_time' => $event[4],
					'end_time' => $event[5],
					'description' => $event[6],
					'all_day' => $event[7]
				);
			}
		} else {
			$stmt->close();
			return false;
		}
	}

	return false;

}

/**
 * Retrieves all events that occur on particular month and year for a specific user
 * @param {Integer} UserID - The ID number of the user
 * @param {Integer} Month - The month to retrieve events for
 * @param {Integer} Year - The year to retrieve events for
 * @return {Array|Integer|Boolean} - Returns an associative array of events. If there are no events,
 *                                   an integer status code 2 is returned. If there was an error,
 *                                   boolean FALSE is returned.
 */
function retrieve_events( $UserID, $Month, $Year ) {

	/* Retrieve the user's calendar ID numbers and put them
	   into a comma separated list. If the user does not have
	   any calendars, then they will not have any events
	   stored in the database. Return status code 2 which
	   symbolizes "No Events Found".
	*/
	if ( $UserCalendarIDs = get_user_calendar_ids( $UserID ) )
		$UserCalendarIDs = implode( ', ', $UserCalendarIDs );
	else
		return 2;

	global $db;

	if ( $stmt = $db->query( "SELECT * FROM event WHERE calendar_id IN($UserCalendarIDs) AND (`date` BETWEEN '$Year-$Month-01' AND LAST_DAY('$Year-$Month-01')) ORDER BY `date`, start_time" ) ) {

		if ( $stmt->num_rows != 0 ) {

			$events = array();

			while ( $event = $stmt->fetch_row() ) {
				array_push( $events, array(
					'ID' => $event[0],
					'calendar_id' => $event[1],
					'name' => $event[2],
					'date' => $event[3],
					'start_time' => $event[4],
					'end_time' => $event[5],
					'description' => $event[6],
					'all_day' => $event[7]
				));
			}

			$stmt->close();
			return ( count( $events ) > 0 ) ? $events : false;

		} else {
			$stmt->close();
			return 2;
		}

		$stmt->close();

	}

	return false;

}

?>