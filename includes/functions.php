<?php
/**
 * Provides useful functions
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

function build_tables( $db ) {

	// This array contains the default structure for each table
	// required by the application
	$tables = array(
		'user' => 'CREATE TABLE IF NOT EXISTS user(
						ID INT NOT NULL AUTO_INCREMENT,
						first_name VARCHAR(35) NOT NULL,
						last_name VARCHAR(35) NOT NULL,
						email VARCHAR(50) NOT NULL,
						password TINYTEXT NOT NULL,
						date_registered DATETIME NOT NULL,
						PRIMARY KEY (ID),
						UNIQUE KEY email (email)
					);',
		'calendar' => 'CREATE TABLE IF NOT EXISTS calendar(
						ID INT NOT NULL AUTO_INCREMENT,
						user_id INT NOT NULL,
						name TINYTEXT NOT NULL,
						description TINYTEXT NOT NULL,
						color TINYINT DEFAULT 0,
						PRIMARY KEY (ID),
						FOREIGN KEY (user_id) REFERENCES user(ID) ON DELETE CASCADE ON UPDATE CASCADE
					);',
		'event' =>	'CREATE TABLE IF NOT EXISTS event(
						ID INT NOT NULL AUTO_INCREMENT,
						calendar_id INT NOT NULL,
						name TINYTEXT NOT NULL,
						`date` DATE NOT NULL,
						start_time TIME NOT NULL,
						end_time TIME NOT NULL,
						description TINYTEXT,
						all_day BOOLEAN DEFAULT 0,
						PRIMARY KEY (ID),
						FOREIGN KEY (calendar_id) REFERENCES calendar(ID) ON DELETE CASCADE ON UPDATE CASCADE
					);',
	);

	// Send the SQL statement to the database so that the
	// tables can be built
	foreach ( $tables as $key => $table ) {
		if ( ! $db->query( $table ) )
			die( 'ERROR: Unable to create table ' . $key  . '.' . mysqli_error($db) );
	}

}