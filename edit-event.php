<?php
/**
 * Displays the "Edit Event" page
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

// Initialize the web application
include( 'includes/core.php' );
include( 'includes/func-calendar.php' );

ob_start();
session_start();

// Application error messages
$ApplicationErrors = array();

// If the user attempting to access this page is not logged in,
// then redirect him or her to the login page.
if ( ! isset( $_SESSION['user_id'] ) )
	header( 'Location: login.php' );

// Retrieve the current user's calendar(s)
$UserCalendarList = get_user_calendars( $_SESSION['user_id'] );

// If the user does not have any calendars, then redirect
// him or her to the calendar/events view page.
if ( count( $UserCalendarList ) == 0 )
	exit( header( 'Location: calendar.php' ) );

// Make sure the URL contains the ID number of an event.
// If it does not, redirect to the calendar/events view page.
if ( isset( $_GET['eventID'] ) && is_int( intval( $_GET['eventID'] ) ) ) {

	// Retrieve event details from the database
	if ( ! $event_details = retrieve_event( intval( $_GET['eventID'] ) , $_SESSION['user_id'] ) )
		array_push( $ApplicationErrors, 'There was a problem retrieving the requested event.' );
	else {

		// If the user pressed the "Save Changes" button, then validate and sanitize the data.
		// If all of the form data is valid, update the event in the database. If the user has
		// not pressed the "Save Changes" button, then populate the form with the requested
		// event's details.
		if ( isset( $_POST['submit'] ) ) {

			// Form error messages
			$FormErrors = array();

			// Form validity flag
			$FormDataValid = true;

			// Form data
			$title = ( isset( $_POST['title'] ) ? $_POST['title'] : '' );
			$date = ( isset( $_POST['date'] ) ? $_POST['date'] : '' );
			$start_time = ( isset( $_POST['start_time'] ) ? $_POST['start_time'] : '' );
			$end_time = ( isset( $_POST['end_time'] ) ? $_POST['end_time'] : '' );
			$description = ( isset( $_POST['description'] ) ? $_POST['description'] : "" );
			$calendar_id = ( isset( $_POST['calendar_id'] ) ? intval( $_POST['calendar_id'] ) : 0 );
			$all_day_event = isset( $_POST['all_day'] );

			// Sanitize all textual form data that isn't validated against a strict regular expression
			$title = filter_var( $title, FILTER_SANITIZE_STRING );
			$description = filter_var( $description, FILTER_SANITIZE_STRING );

			// Make sure the user entered a name for the event
			if ( strlen( $title ) == 0 ) {
				$FormDataValid = false;
				array_push( $FormErrors, 'You forgot to enter a name for the event.' );
			}

			// Validate start date
			if ( ! is_date_valid( $date ) ) {
				$FormDataValid = false;
				array_push( $FormErrors, 'You have entered an invalid date.' );
			}

			// Only validate the start and end time if the user did not tick
			// the 'All Day' checkbox.
			if( ! $all_day_event ) {

				// Validate start time
				if ( ! is_time_valid( $start_time ) ) {
					$FormDataValid = false;
					array_push( $FormErrors, 'You have entered an invalid start time.' );
				}

				// Validate start time
				if ( ! is_time_valid( $end_time ) ) {
					$FormDataValid = false;
					array_push( $FormErrors, 'You have entered an invalid end time.' );
				}
			}

			// If the sanitized description entered by the user is a zero-length string, 
			// it means they probably entering HTML code.
			if ( strlen( $description ) == 0 && ( isset( $_POST['description'] ) && strlen( $_POST['description'] ) > 0 ) )
				array_push( $FormErrors, 'You entered HTML code into the description box. Please refrain from using any HTML.' );

			// Make sure the user selected a calendar that he/she owns. No user is allowed to
			// update another user's event.
			if ( ! is_calendar_owner( $calendar_id , $_SESSION['user_id'] ) ) {

				$FormDataValid = false;

				// Use a generic error message. A malicious user should never be given too much information.
				array_push( $FormErrors, 'You must select a calendar to insert the event into.' );

			}

			// If the user-submitted form data is valid, then save it to the database.
			if ( $FormDataValid ) {

				// Build the event array. If the event is an "All Day" event, set the start and 
				// end times to span entire day. Otherwise, build the event array using all of the
				// data provided by the user.
				if ( $all_day_event ) {

					$event = array(
						0 => $title,
						1 => date( 'Y-m-d', strtotime( $date ) ),
						2 => '00:00',
						3 => '23:59',
						4 => $description,
						5 => 1,
						6 => $calendar_id
					);

				} else {

					$event = array(
						0 => $title,
						1 => date( 'Y-m-d', strtotime( $date ) ),
						2 => date( 'H:i', strtotime( $start_time ) ),
						3 => date( 'H:i', strtotime( $end_time ) ),
						4 => $description,
						5 => 0,
						6 => $calendar_id
					);

				}

				// Attempt to update the event in the database. If there is a problem, display an error message.
				// Otherwise, redirect the user to the calendar/events view page.
				if ( update_event( intval( $_GET['eventID'] ), $event ) )
					exit( header( 'Location: calendar.php' ) );
				else
					array_push( $ApplicationErrors, 'There was a problem updating your event.' );
				
			}

		// If the user has not pressed the "Save Changes" button, then populate the form fields
		// with the event meta data retrieved from the database.
		} else {

			$title = $event_details['name'];
			$date = date( 'm/d/Y', strtotime( $event_details['date'] ) );
			$all_day_event = $event_details['all_day'];

			// If the event is an "All Day" event, display empty start and end time
			// text boxes. The user should not be aware that this web application
			// automatically sets these fields if the "All Day" checkbox is ticked.
			if ( $all_day_event == 0 ) {
				$start_time = date( 'g:i A', strtotime( $event_details['start_time'] ) );
				$end_time = date( 'g:i A', strtotime( $event_details['end_time'] ) );
			} else {
				$start_time = '';
				$end_time = '';
			}

			$description = $event_details['description'];
			$calendar_id = $event_details['calendar_id'];

		}
	}
}
else
	exit( header( 'Location: calendar.php' ) );

?>
<!DOCTYPE html>
<html lang="en" class="default">
	<head>
		<meta charset="UTF-8">
		<title>Calendr - Modify Event</title>
		<meta name="description" content="Calendr is a free, easy-to-use event management system designed to help you keep track of life's important events.">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Style Sheets -->
		<link rel="stylesheet" href="css/style-backend.css" />
		<link rel="stylesheet" href="css/jquery-ui.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
		<!-- Scripts -->
		<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
		<script src="js/jquery-ui.js"></script>
		<script>
			$(function() {
				$( "#date" ).datepicker();
			});
		</script>
	</head>
	<body>
		<div id="topbar" class="clearfix">
			<a href="/calendar.php">
				<img src="/img/logo-small.png" alt="" width="118" height="28">
			</a>
			<ul id="toolbar"><li><a href="#">&nbsp;</a></li></ul>
			<ul id="account">
				<li>
					<a href="#"><?php echo $_SESSION['user_name']; ?></a>
				</li>
			</ul>
		</div>
		<div id="content">
			<?php

			// Output all application-related error messages to the screen
			if ( isset( $ApplicationErrors ) && count( $ApplicationErrors ) > 0 ) {
				echo	'<div class="error-banner">
							<div>
								<p>The following errors occurred:</p>
								<ul>';
				
				foreach ( $ApplicationErrors as $Error )
					echo '<li>' . $Error . '</li>';
				
				echo 			'</ul>
						</div>
					</div>';
			}

			// Output all form-related error messages to the screen
			if ( isset( $FormErrors ) && count( $FormErrors ) > 0 ) {
				echo	'<div class="error-banner">
							<div>
								<p>Please correct the following issues:</p>
								<ul>';
				
				foreach ( $FormErrors as $Error )
					echo '<li>' . $Error . '</li>';
				
				echo 			'</ul>
						</div>
					</div>';
			}

			?>
			<div class="page-heading">Modify Event</div>
			<form method="post" action="/edit-event.php<?php if ( isset( $_GET['eventID'] ) ) echo '?eventID=' . intval( $_GET['eventID'] ); ?>">
				<ul>
					<li>
						<label for="title">Name</label>
						<input type="text" id="title" name="title" autocomplete="off"
						<?php if ( isset( $title ) ) echo "value='" . $title . "'"; ?>>
					</li>
					<li><label>&nbsp;</label><hr></li>
					<li>
						<label for="date">Date</label>
						<input type="text" id="date" name="date" autocomplete="off"
						<?php if ( isset( $date ) ) echo "value='" . $date . "'"; ?>>
					</li>
					<li><label>&nbsp;</label><hr></li>
					<li>
						<label for="start_time">Start Time</label>
						<input type="text" id="start_time" name="start_time" autocomplete="off"
						<?php if ( isset( $start_time ) ) echo "value='" . $start_time . "'"; ?>>
					</li>
					<li>
						<label for="end_time">End Time</label>
						<input type="text" id="end_time" name="end_time" autocomplete="off"
						<?php if ( isset( $end_time ) ) echo "value='" . $end_time . "'"; ?>>
					</li>
					<li><label>&nbsp;</label><hr></li>
					<li>
						<label for="end_time">Description</label>
						<textarea id="description" name="description" rows="5"><?php if ( isset( $description ) ) echo $description; ?></textarea>
					</li>
					<li>
						<label for="calendar_list">Calendar</label>
						<select id="calendar_list" name="calendar_id">
						<?php
							// Populate the dropdown list with the user's calendars.
							// If the user previously pressed the "Create Event" button, automatically reselect their choice
							// (by adding the 'selected' attribute to the corresponding dropdown item).
							foreach ( $UserCalendarList as $Calendar )
							echo '<option value="' . $Calendar[0] . '" ' . ( ( $Calendar[0] == $calendar_id ) ? 'selected' : null )  . ' >' . $Calendar[1] . '</option>'
						?>
						</select>
					</li>
					<li>
						<label>&nbsp;</label>
						<input type="checkbox" name="all_day" id="all_day" value="1" <?php echo ( isset( $all_day_event ) && $all_day_event == 1 ) ? 'checked' : null; ?>>
						<label for="all_day" id="all_day_event">All Day</label>
					</li>
				</ul>
				<input type="submit" name="submit" class="button button-primary" value="Save Changes">
				<a href="/calendar.php" class="button button-secondary">Cancel</a>
			</form>
		</div>
	</body>
</html>