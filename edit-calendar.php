<?php
/**
 * Displays the "Modify Calendar" page
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

// Initialize the web application
include( 'includes/core.php' );
include( 'includes/func-calendar.php' );

ob_start();
session_start();

// Application error messages
$ApplicationErrors = array();

// If the user attempting to access this page is not logged in,
// then redirect them to the login page.
if ( ! isset( $_SESSION['user_id'] ) )
	exit( header( 'Location: login.php' ) );

// Make sure the URL contains the ID number of a calendar.
// If it does not, redirect to the calendar/events view page.
if ( isset( $_GET['id'] ) && is_int( intval( $_GET['id'] ) ) ) {

	// Retrieve the calendar details
	if ( ! $calendar = get_calendar( intval( $_GET['id'] ), $_SESSION['user_id'] ) ) {
		// There was a problem fetching the calendar details from the database
		array_push( $ApplicationErrors, 'There was a problem retrieving your calendar.' );
		// Set the $cal_color variable a number that does not correspond to any legitimate color choice.
		// This prevents error messages from covering the screen.
		$cal_color = -1;
	}
	else {

		// If the user pressed the "Save Changes" button, then validate and sanitize the data.
		// If all of the form data is valid, update the calendar's information in the database.
		// If the user has not pressed the "Save Changes" button, then populate the form with
		// the requested calendar's details.
		if ( isset( $_POST['submit'] ) ) {

			// Form data
			$cal_name = ( isset( $_POST['name'] ) ? $_POST['name'] : "" );
			$cal_desc = ( isset( $_POST['description'] ) ? $_POST['description'] : "" );
			$cal_color = ( isset( $_POST['color'] ) ? intval( $_POST['color'] ) : -1 );

			// Sanitized form data
			$cal_name = filter_var( $cal_name, FILTER_SANITIZE_STRING );
			$cal_desc = filter_var( $cal_desc, FILTER_SANITIZE_STRING );

			// Form validity flag
			$FormDataValid = true;

			// Form error messages
			$FormErrors = array();

			// Make sure the user entered a valid calendar name
			if ( strlen( $cal_name ) == 0 ) {
				$FormDataValid = false;
				array_push( $FormErrors, 'You forgot to enter a name for your calendar.' );
			}

			// Make sure the user choose a valid calendar color
			if ( $cal_color < 0 || $cal_color > 6 ) {
				$FormDataValid = false;
				array_push( $FormErrors, 'You forgot to pick a color for the calendar.' );
			}

				// Make sure the description entered by the user was not sanitized to the point
				// where the description became a zero-length string.
				if ( strlen( $cal_desc ) == 0 & ( isset( $_POST['description'] ) && strlen( $_POST['description'] ) > 0 ) )
					array_push( $FormErrors, 'You entered HTML code into the description box. Please refrain from using any HTML tags.' );

			// If the user-submitted form data is valid, then save it to the database.
			if ( $FormDataValid ) {

				// Attempt to update the user's calendar in the database. If there is a problem, display an error message.
				// Otherwise, redirect the user to the calendar/events view page.
				if ( update_calendar( $cal_name, $cal_desc, $cal_color, intval( $_GET['id'] ) ) )
					exit( header( 'Location: calendar.php' ) );
				else
					array_push( $ApplicationErrors, 'There was a problem updating your calendar.' );

			}

		// If the user has not pressed the "Save Changes" button, then populate the form fields
		// with the calendar meta data retrieved from the database.
		} elseif ( isset( $calendar ) ) {

			$cal_name = $calendar[0];
			$cal_desc = $calendar[1];
			$cal_color = $calendar[2];

		}
	}
}
else
	exit( header( 'Location: calendar.php' ) );

?>
<!DOCTYPE html>
<html lang="en" class="default">
	<head>
		<meta charset="UTF-8">
		<title>Calendr - Modift Calendar</title>
		<meta name="description" content="Calendr is a free, easy-to-use event management system designed to help you keep track of life's important events.">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Style Sheets -->
		<link rel="stylesheet" href="css/style-backend.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="topbar" class="clearfix">
			<a href="/calendar.php">
				<img src="/img/logo-small.png" alt="" width="118" height="28">
			</a>
			<ul id="toolbar"><li><a href="#">&nbsp;</a></li></ul>
			<ul id="account">
				<li>
					<a href="#"><?php echo $_SESSION['user_name']; ?></a>
				</li>
			</ul>
		</div>
		<div id="content">
			<?php

			// Output all application-related error messages to the screen
			if ( isset( $ApplicationErrors ) && count( $ApplicationErrors ) > 0 ) {
				echo	'<div class="error-banner">
							<div>
								<p>The following errors occurred:</p>
								<ul>';
				
				foreach ( $ApplicationErrors as $Error )
					echo '<li>' . $Error . '</li>';
				
				echo 			'</ul>
						</div>
					</div>';
			}

			// Output all form-related error messages to the screen
			if ( isset( $FormErrors ) && count( $FormErrors ) > 0 ) {
				echo	'<div class="error-banner">
							<div>
								<p>Please correct the following issues:</p>
								<ul>';
				
				foreach ( $FormErrors as $Error )
					echo '<li>' . $Error . '</li>';
				
				echo 			'</ul>
						</div>
					</div>';
			}

			?>
			<div class="page-heading">Modify Calendar</div>
			<form method="post" action="/edit-calendar.php<?php if ( isset( $_GET['id'] ) ) echo '?id=' . intval( $_GET['id'] ); ?>">
				<ul>
					<li>
						<label for="name">Name</label>
						<input type="text" id="name" name="name"
						<?php if ( isset( $cal_name ) ) echo "value='" . $cal_name . "'"; ?>>
					</li>
					<li>
						<label for="description">Description</label>
						<textarea name="description" id="description" rows="5"><?php if ( isset( $cal_desc ) ) echo $cal_desc; ?></textarea>
					</li>
					<li>
						<label>Color</label>
						<span class="color-palette">
							<input type="radio" name="color" value="0" <?php if ( $cal_color == 0 ) echo 'checked'; ?>>
							<span class="cal-color-0">&nbsp;</span>
						</span>
						<span class="color-palette">
							<input type="radio" name="color" value="1" <?php if ( $cal_color == 1 ) echo 'checked'; ?>>
							<span class="cal-color-1">&nbsp;</span>
						</span>
						<span class="color-palette">
							<input type="radio" name="color" value="2" <?php if ( $cal_color == 2 ) echo 'checked'; ?>>
							<span class="cal-color-2">&nbsp;</span>
						</span>
						<span class="color-palette">
							<input type="radio" name="color" value="3" <?php if ( $cal_color == 3 ) echo 'checked'; ?>>
							<span class="cal-color-3">&nbsp;</span>
						</span>
						<span class="color-palette">
							<input type="radio" name="color" value="4" <?php if ( $cal_color == 4 ) echo 'checked'; ?>>
							<span class="cal-color-4">&nbsp;</span>
						</span>
						<span class="color-palette">
							<input type="radio" name="color" value="5" <?php if ( $cal_color == 5 ) echo 'checked'; ?>>
							<span class="cal-color-5">&nbsp;</span>
						</span>
					</li>
				</ul>
				<input type="submit" class="button button-primary" name="submit" value="Save Changes">
				<a href="/calendar.php" class="button button-secondary">Cancel</a>
			</form>
		</div>
	</body>
</html>