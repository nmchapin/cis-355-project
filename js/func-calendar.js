/**
 * Contains client-side functions for calendar.php
 *
 * @package		Simple Event Management System (SEMS)
 * @author		Nicholas Chapin <nmchapin@svsu.edu>
 * @license		GPLv3 <http://www.gnu.org/copyleft/gpl.html>
 * @version		1.0
 *
 */

// =============== Event Handlers =============== //

// Triggers when the web page finishes loading
$( document ).ready(function() {

	// Populate the calendar view with the user's events
	retrieve_user_events( get_current_calendar_month(), get_current_calendar_year() );

	// Retrieve the color associated with the current user's calendar
	CalendarColors = getCalendarColors();

	// Triggers when the user clicks the dropdown button on a calendar list item
	$( 'ul.cal-dropdown li' ).click(function() {

		if ( ! $( 'ul.cal-dropdown ul' ).length > 0 ) {
			$( this ).append(
				'<ul> \
					<li class="modify">Modify</li> \
					<li class="delete">Delete</li> \
				</ul>'
			);
		}

		$( this ).children( 'ul' ).addClass( 'clicked' );

	});

	// Triggers when the user's mouse leaves the dropdown menu
	$( '#sidebar > ul > li' ).mouseleave(function() {

		$( this ).find( 'ul.clicked' ).remove();
	
	});

	// Triggers when the user clicks on the "Modify" calendar menu item
	$( 'ul.cal-dropdown li' ).on( 'click', 'li.modify', function() {

		// Get the ID number of the calendar
		var CalendarID = $( this ).parents( 'li.cal-item' ).attr( 'id' ).match(/\d+/);

		if ( parseInt( CalendarID[0] ) )
			window.location.href = '/edit-calendar.php?id=' + CalendarID[0];

	});

	// Triggers when the user clicks on the "Delete" calendar menu item
	$( 'ul.cal-dropdown li' ).on( 'click', 'li.delete', function() {

		// Get the ID number of the calendar
		var CalendarID = $( this ).parents( 'li.cal-item' ).attr( 'id' ).match(/\d+/);
		var CalendarName = $( this ).parents( 'li.cal-item' ).children( 'span.cal-name' ).text();

		// Confirm delete with the user
		if ( window.confirm( 'WARNING: This will erase all events associated with the calendar.\n\nAre you sure you want to delete the calendar "' + CalendarName + '"?' ) ) {
			delete_calendar( CalendarID );

			// Remove all related calendar events from the monthly calendar view
			$( 'div.week-day div.event.cal-id-' + CalendarID ).remove();
		}

	});

	/**
	 * When the user clicks on an event, redirect them to the event view page
	 */
	$( 'div.week-day' ).on('click', 'div.event', function() {
		
		// Get the current domain name of the site
		var location = 'http://' + document.domain;

		// Get the event ID
		var eventID = $(this).attr( 'id' );
		eventID = eventID.substring( 6, eventID.length );
		
		// Create link to the "View Event" page
		location += '/view-event.php?eventID=' + eventID.toString();
		
		// Navigate to the "View Event" page
		window.location.href = location;

	});

});

// ============================================== //

/**
 * Sends an AJAX request to the server asking for a particular calendar to be deleted
 *
 * NOTE: For security reasons, a user can only request that a calendar they own be deleted.
 *       All requests that try to delete another user's calendar will be ignored. The server
 *       uses session cookies to verify the legitimacy of all requests.
 *
 * @param {Number} CalendarID - The ID number of the calendar to delete
 */
function delete_calendar( CalendarID ) {

	$.ajax({
		type: 'POST',
		url: 'includes/ajax.php',
		data: { 
			'function': 'delete_calendar',
			'calendar_id': parseInt( CalendarID )
		},
		success: function(data) {
			if ( data != '1' )
				alert( 'There was a problem deleting your calendar.' );
			else
				remove_calendar_from_list( CalendarID );
		}
	});

}
/**
 * Sends an AJAX request to the server asking for a JSON feed that contains all of the
 * user's events for a particular month and year.
 *
 * NOTE: For security reasons, a user can only request a listing of events from the calendars
 *       they own. All requests that attempt to retrieve events from another user's calendar
 *       will be ignored. The server uses session cookies to verify the legitimacy of all
 *       requests.
 *
 * @param {Number} Month - The number of the month to retrieve events for
 * @param {Number} Year - The year to retrieve events for
 */
function retrieve_user_events( Month, Year ) {

	var date = new Date( Year.toString() + '/' + Month.toString() + '/1' );

	$.ajax({
		type: 'POST',
		url: 'includes/ajax.php',
		data: { 
			'function': 'retrieve_events',
			'month': Month,
			'year': Year
		},
		success: function(data) {
			if ( data === '0' )
				alert( 'There was a problem retrieving your events.' );
			else if ( data === '2' )
				console.log( 'No events to display' );
			else
				populateCalendar( data, date );
		}
	});

}
/**
 * Removes a calendar from the "Personal Calendars" list
 * @param {Number} CalendarID - The ID number of the calendar to remove
 */
function remove_calendar_from_list( CalendarID ) {

	// Remove calendar from the sidebar
	$( '#cal-id-' + CalendarID ).remove();

	// If there no calendars currently displayed in the sidebar,
	// display a "Getting Started" message and remove the "New Event"
	// button from the toolbar.
	if ( $('#sidebar > ul > li').length == 0 ) {
		$( '#toolbar li:first-child' ).remove();
		$( '#sidebar' ).append( '<p>Click the "New Calendar" button above to create your first calendar.</p>' );
	}
}
/**
 * Retrieves the year that is currently displayed in the monthly calendar view
 * @returns {Number} - The year that is currently displayed in the monthly calendar view
 */
function get_current_calendar_year() {

	var year = $( '#calendar-title' ).text().match(/\d+/);
	year = year === null ? null : year[0]; 
	return ( isInt( year ) ) ? parseInt( year ) : -1;

}
/**
 * Retrieves the number of the month that is currently displayed in the monthly calendar view
 * @returns {Number} - The number of the month displayed in the monthly calendar view
 */
function get_current_calendar_month() {

	var month = $( '#calendar-title' ).text().match(/[A-Za-z]+/);
	month = month === null ? null : month[0]; 
	return getMonthNumber( month );


}
/**
 * Retrieves the color ID of each calendar in the "Personal Calendars" list
 * @returns {Array} - An array containing containing elements where each key is
 *                    the ID number of a calendar its value is the calendar's
 *                    associated color ID.
 */
function getCalendarColors() {

	var CalendarColors = [];

	$( '#sidebar > ul li.cal-item' ).each(function() {

		// Stores the ID number of a calendar listed in the Personal Calendars" list
		var CalendarID = $( this ).attr( 'id' ).match(/\d+/);
		// Stores the ID number of the color associated with CalendarID (the variable seen above)
		var CalendarColorID = $( this ).children( 'span.cal-color' ).attr( 'class' ).match(/\d+/);

		// Make sure the variables are arrays. If the variables are not arrays, that means the
		// elements retrieved (see CalendarID & CalendarColorID variables above) do not exist in
		// the DOM.
		if ( Array.isArray( CalendarID ) && Array.isArray( CalendarColorID ) ) {

			// Make sure the first index of the array is the ID number of the calendar and
			// the color respectively. If not, assign -1 to show that something went wrong.
			CalendarID = isInt( CalendarID[0] ) ? CalendarID[0] : -1;
			CalendarColorID = isInt( CalendarColorID[0] ) ? CalendarColorID[0] : -1;

			// Add the calendar ID as an index to the array. Assign the item
			// the color ID that is associated with the calendar ID.
			if ( parseInt( CalendarID ) != -1 )
				CalendarColors[ parseInt( CalendarID ) ] = parseInt( CalendarColorID );
		}

	});

	return CalendarColors;
}
/**
 * Retrieves the color ID assigned to a particular calendar
 * @param {Number} CalendarID - The ID number of a calendar
 * @returns {Number} - The ID number of the color assigned to a particular calendar
 */
function getCalendarColorID( CalendarID ) {

	if ( $.inArray( CalendarID, CalendarColors ) )
		return CalendarColors[CalendarID];

	return -1;

}

/**
 * Determines whether a variable is an integer or not
 * @param {Object} value - The variable to be tested
 * @returns {Boolean} - Returns TRUE if the variable is an integer, FALSE otherwise
 */
function isInt( value ) {

	return !isNaN( value ) && (function(x) { return (x | 0) === x; })( parseFloat( value ) );

}
/**
 * Populates the monthly calendar view with events
 * @param {JSON} jsonEvents - A JavaScript object containing a list of events
 * @param {Date} date - The month and year to output events for
 */
function populateCalendar( jsonEvents, date ) {

	// Convert JSON feed into JavaScript object
	events = jQuery.parseJSON(jsonEvents);

	// Counter for the week number
	// NOTE: The first week of the month starts at index number 2. Index number 1 is reserved for the week day headings (Mon - Sat).
	var calendar_week = 2;
	// Counter for the day number
	var day_number = 1;

	// Loop through each day in the month
	while( jQuery("div.week-days:nth-child("+calendar_week+")").length > 0 ) {
		jQuery( "div.week-days:nth-child("+calendar_week+") div:not(.empty-day)" ).each(function() {
				date.setDate( day_number );
				outputEvents( events, date, jQuery( this ) );
				day_number++;
			}
		);
		calendar_week++;
	}
}
/**
 * Outputs events to the DOM for a particular month, day, and year
 * @param {Object} events - A JavaScript object containing the list of events
 * @param {Date} date - The month, day, and year to output events for
 * @param {DOM} element - The DOM object to append event HTML to
 */
function outputEvents( events, date, element ) {

	var day = zeroPad( date.getDate().toString(), 2 );
	var month = zeroPad( ( date.getMonth() + 1 ).toString(), 2 );
	var year = date.getFullYear().toString();

	// Loop through the entire events list
	for ( var index = 0; index < events.length; index++ ) {
		// Check if an event starts on a particular day and, if it does, print it out
		if ( events[index].date == ( year + '-' + month + '-' + day ) ) {

			// Check if event is an all day event
			if ( events[index].all_day == '0' )
				$( element ).append( '<div id="event-' + events[index].ID + '" class="event cal-color-' + getCalendarColorID(events[index].calendar_id) + ' cal-id-' + events[index].calendar_id + '">' + formatTime( events[index].start_time ) + ' ' + events[index].name + '</div>' );
			else
				$( element ).append( '<div id="event-' + events[index].ID + '" class="event cal-color-' + getCalendarColorID(events[index].calendar_id) + ' cal-id-' + events[index].calendar_id + '">' + events[index].name + '</div>' );
		}
	};
}
/**
 * Converts a short 24-hour time string into a short 12-hour time string
 * @param {String} time - A time string in 24-hour format
 * @returns {String} - A time string in 12-hour format
 */
function formatTime( time ) {

	var hours = parseInt( time[0]+time[1] );
	var minutes = time[3]+time[4];
	var period = '';

	// Determine the time period
	if ( hours == 12 )
		period = 'pm';
	else if ( hours > 12 ) {
		period = 'pm';
		hours -= 12;
	}
	else {
		if ( hours == 0 )
			hours = 12;
		period = 'am';
	}

	// Do not display minutes if the hour is sharp
	if ( minutes == '00' )
		return hours.toString() + period;
	else
		return hours.toString() + ':' + minutes + period;

}
/**
 * Returns a new string of a specified length with leading zeros
 * @param {String} str - An unpadded string
 * @param {Number} length - The desired length of the padded string
 * @returns {Number} - A left-padded string with leading zeros
 */
function zeroPad( str, length ) {

	while ( str.length < length ) str = '0' + str;
	return str;

}
/**
 * Returns the number that represents a particular month
 * @param {String} month - An unabbreviated month name
 * @returns {Number} - A number representing the given month
 */
function getMonthNumber( month ) {
	switch(month) {
		case 'January':
			return 1;
		case 'February':
			return 2;
		case 'March':
			return 3;
		case 'April':
			return 4;
		case 'May':
			return 5;
		case 'June':
			return 6;
		case 'July':
			return 7;
		case 'August':
			return 8;
		case 'September':
			return 9;
		case 'October':
			return 10;
		case 'November':
			return 11;
		case 'December':
			return 12;
		default:
			return -1;
	};
}